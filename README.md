# magdalene-black
## a vibrant black theme for Visual Studio in green and red

## Purpose

My eyes are kind of sensitive, and I really appreciate a good black theme, but
there are three issues I regularly run into:

* most dark themes are dark or medium gray backgrounds, not black, which
needlessly lowers the contrast and, thus, the visibility. 
* a lot of themes *really* dim the comments, which is the opposite of what
I want. I don't just put comments in arbitrarily to be ignored. If there's a 
comment, I want it to jump out at me. 
* a lot of dark themes don't use a variety of vibrant colors. I suspect this
is because using different shades of the same few colors is aesthetically 
pleasing and easy to make accessible to color blind folks, but for me it 
makes it unnecessarily difficult to skim. I prefer a vibrant theme. 

One theme I've always really appreciated but never really had a name for is 
just the default emacs theme on an Ubuntu black terminal--red comments, 
green identifiers, white text. (If someone knows what this is called, please
do let me know.)

## Description

This theme uses a truly black background with a green accent, using red for
comments. 

## Accessibility concerns

While accessibility in my web projects is important to me, this particular
project is for me to solve a particular problem I have, which is that I want 
to take full advantage of my color vision on a black background. For me, 
reds and greens are two of the easiest colors to distinguish, so I do not
forsee a version of this plugin which is accessible to color-blind people. I
do hope you understand, and if you would like any help forking this project, 
please let me know. 
